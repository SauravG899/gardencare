#include "DHT.h"

#include <SoftwareSerial.h>

#define DHTPIN 5                       // Digital Pin 5 for DHT

#define DHTTYPE DHT11                  

String apiKey = "*****";    // API Key of thingspeak account

String Host_Name = "-------";   //WIFI network ID      

String Password = "--------";          // WIFI Password

SoftwareSerial ser(2, 3);              // RX, TX

int i=1;

DHT dht(DHTPIN, DHTTYPE);              

void setup() {                

Serial.begin(115200);                  

ser.begin(115200);                     

ser.println("AT+RST");               

dht.begin();                        
char inv ='"';

String cmd = "AT+CWJAP";

       cmd+= "=";

       cmd+= inv;

       cmd+= Host_Name;

       cmd+= inv;

       cmd+= ",";

       cmd+= inv;

       cmd+= Password;

       cmd+= inv;

ser.println(cmd);                    // Connecting ESP8266 to your WiFi Router

  }

// the loop 

void loop() {

  int moisture = analogRead(A1);
 
  int humidity =  dht.readHumidity();             // Reading Humidity Value

  int temperature = dht.readTemperature();        // Reading Temperature Value

  String state1=String(humidity);                 // Converting them to string to send to the API 

  String state2=String(temperature);              

  String state3=String(moisture);
 

  String cmd = "AT+CIPSTART=\"TCP\",\"";          // Establishing TCP connection

  cmd += "----------------";                       // api.thingspeak.com

  cmd += "\",80";                                 // port 80

  ser.println(cmd);

  Serial.println(cmd);

 if(ser.find("Error")){

    Serial.println("AT+CIPSTART error");

    return;

  }

String getStr = "GET /update?api_key=";         // prepare GET string

  getStr += apiKey;

  getStr +="&field1=";

  getStr += String(state1);                       // Humidity Data

  getStr +="&field2=";

  getStr += String(state2);                       // Temperature Data
  
  getStr += "&field3=";
  
  getStr += String(state3);
   getStr += "\r\n\r\n";

  cmd = "AT+CIPSEND=";

  cmd += String(getStr.length());                // Total Length of data

  ser.println(cmd);

  Serial.println(cmd);

 

if(ser.find(">")){

    ser.print(getStr);

    Serial.print(getStr);

  }

  else{

    ser.println("AT+CIPCLOSE");                  // closing connection

    // alert user

    Serial.println("AT+CIPCLOSE");

  }

 delay(1000);                                  // Update after every 15 seconds

}
